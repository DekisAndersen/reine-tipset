const process = require('process')
const { query, Client } = require('faunadb')
const fetch = require('node-fetch');
// FQL functions
const {
  Ref,
  Paginate,
  Get,
  Match,
  Select,
  Exists,
  Index,
  Update,
  Create,
  Collection,
  Documents,
  Delete,
  Join,
  Var,
  Call,
  Map,
  Lambda,
  Function: Fn
} = query;

let client = null;
const BASE_URL = process.env.BASE_URL;

const INVALIDATE_AFTER = 600000; // 10 min

/**
 * Copied from logic/scores.ts
 * @param bet
 * @param match
 * @returns {null|number|number}
 */
const calcMatchScore = (bet, match) => {
  if (match.status === 'SCHEDULED') {
    return null;
  }
  if (bet == null) {
    return 0;
  }
  const betHome = Number(bet.home);
  const betAway = Number(bet.away);
  if (Number.isNaN(betHome) || Number.isNaN(betAway)) {
    return 0;
  }
  let matchHome = match.score.fullTime.homeTeam;
  let matchAway = match.score.fullTime.awayTeam;
  if (match.score.extraTime.homeTeam != null) {
    const etHome = match.score.extraTime.homeTeam;
    const etAway = match.score.extraTime.awayTeam;
    matchHome = matchHome - etHome;
    matchAway = matchAway - etAway;
  }
  if (match.score.penalties.homeTeam != null) {
    const pHome = match.score.penalties.homeTeam;
    const pAway = match.score.penalties.awayTeam;
    matchHome = matchHome - pHome;
    matchAway = matchAway - pAway;
  }
  
  if (betHome === matchHome && betAway === matchAway) {
    return 3;
  }
  return Math.sign(betHome - betAway) === Math.sign(matchHome - matchAway) ? 1 : 0;
}

function generateHeaders(token) {
  const headers = { 'Content-Type': 'application/json' };
  return { ...headers, Authorization: `Bearer ${token}` };
}

const fetchFinishedMatches = async (token) => {
  const headers = generateHeaders(token);
  const response = await fetch(BASE_URL + '/api/football?api=matches&params=status=FINISHED', {
    headers
  });
  if (!response.ok) {
    // NOT res.status >= 200 && res.status < 300
    return { statusCode: response.status, body: response.statusText }
  }
  const data = await response.json();
  return data.matches;
}

const fetchCompetition = async (token) => {
  const headers = generateHeaders(token);
  const response = await fetch(BASE_URL + '/api/football?api=competition', {
    headers
  });
  if (!response.ok) {
    // NOT res.status >= 200 && res.status < 300
    return { statusCode: response.status, body: response.statusText }
  }
  const data = await response.json();
  return data;
}

const createUserInMap = (scoreMap, userRef) => {
  if (scoreMap[userRef] === undefined) {
    scoreMap[userRef] = {
      total: 0,
      threes: 0,
      ones: 0,
      guessedWinner: false
    };
  }
}

const addMatchScores = async (scoreMap, match) => {
  const matchBets = await client.query(Paginate(Match(Index('bets_per_match'), match.id)));
  const itemRefs = matchBets.data;
  // create new query out of item refs.
  const getAllItemsDataQuery = itemRefs.map((ref) => query.Get(ref));
  // then query the refs
  const bets = await client.query(getAllItemsDataQuery)
  bets.forEach(bet => {
    const userRef = bet.data.user;
    const matchScore = calcMatchScore(bet.data.bet, match);
    if (scoreMap[userRef] === undefined) {
      createUserInMap(scoreMap, userRef);
    }
    const score = scoreMap[userRef];
    score.total = score.total + matchScore;
    if (matchScore === 3) {
      score.threes++;
    }
    else if (matchScore === 1) {
      score.ones++;
    }
    scoreMap[userRef] = score;
  });
}

const getAllDBUsers = async () => {
  const dbUsers = await client.query(Paginate(Documents(Collection('users'))));
  const itemRefs = dbUsers.data
  // create new query out of item refs. http://bit.ly/2LG3MLg
  const getAllItemsDataQuery = itemRefs.map((ref) => query.Get(ref))
  // then query the refs
  const users = await client.query(getAllItemsDataQuery)
  return users;
}

const addWinnerScore = async (scoreMap, winner, users) => {
  users.forEach(user => {
    if (user.data.winner && user.data.winner.id === winner.id) {
      const score = scoreMap[user.ref.id];
      score.total = score.total + 10;
      score.guessedWinner = true;
      scoreMap[user.ref.id] = score;
    }
  });
};

const getScoreData = async (token) => {
  // Check existing
  const currentScores = await client.query(Paginate(Documents(Collection('scores')), { size: 1 }));
  if (currentScores && currentScores.data && currentScores.data.length) {
    const dbScore = await client.query(Get(currentScores.data[0]));
    const now = new Date().getTime();
    if ((now - dbScore.data.storedAt <= INVALIDATE_AFTER)) {
      return dbScore.data;
    }
    else {
      // Delete old!
      console.log('Highscore cache old, delete and re-create');
      await client.query(Map(
        Paginate(
          Documents(Collection('scores'))
        ),
        Lambda(
          ['ref'],
          Delete(Var('ref'))
        )
      ));
    }
  }
  let finishedMatches = await fetchFinishedMatches(token);
  if (finishedMatches.statusCode) {
    return finishedMatches;
  }
  
  const scoreMap = {};
  // Iterate, get all bets per match
  for (const match of finishedMatches) {
    // Add to score-map
    await addMatchScores(scoreMap, match);
  }
  
  // Ensure all users are added
  let allDBUsers = await getAllDBUsers();
  allDBUsers.forEach(user => {
    createUserInMap(scoreMap, user.ref.id)
  })
  
  
  // Check winner scores
  let competition = await fetchCompetition(token);
  let winner = competition.currentSeason.winner;
  
  if (winner != null) {
    await addWinnerScore(scoreMap, winner, allDBUsers)
  }
  
  // Store map in DB
  const storedAt = new Date().getTime();
  const data = {
    storedAt,
    scoreMap
  };
  await client.query(Create(Collection('scores'), { data }));
  return data;
}

const generateHighscores = async (token) => {
  const scoreData = await getScoreData(token);
  const getAllUsersQuery = Object.keys(scoreData.scoreMap).map((refId) => Get(Ref(Collection('users'), `${refId}`)));
  const allUsers = await client.query(getAllUsersQuery);
  const highscores = [];
  allUsers.forEach(user => {
    const data = {
      user: user.data,
      score: {
        ...scoreData.scoreMap[user.ref.id]
      }
    };
    highscores.push(data);
  })
  return highscores;
};

const handler = async function(event, context) {
  console.log('Highscore - protected function!')
  client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET
  });
  // Reading the context.clientContext will give us the current user
  const user = context.clientContext && context.clientContext.user
  
  if (!user) {
    console.log('No claims! Begone!')
    return {
      statusCode: 401,
      body: JSON.stringify({
        data: 'NOT ALLOWED'
      })
    }
  }
  
  try {
    const response = await generateHighscores(context.clientContext.identity.token)
    return {
      statusCode: 200,
      body: JSON.stringify({
        data: response
      })
    }
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: 500,
      body: JSON.stringify({
        data: err
      })
    }
  } finally {
    console.log('highscore client close');
    client.close().finally(); // Not waiting for the close connection.
  }
  
}

module.exports = { handler }
