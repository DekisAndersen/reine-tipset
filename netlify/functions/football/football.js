const process = require('process')
const fetch = require('node-fetch')
const { query, Client } = require('faunadb')
const q = query

let client = null;

const INVALIDATE_AFTER = 600000; // 10 minutes
const COMPETITION = 'EC'
const API_PATHS = {
  matches: `competitions/${COMPETITION}/matches`,
  match: `matches/`,
  competition: `competitions/${COMPETITION}`,
  standings: `competitions/${COMPETITION}/standings`,
  teams: `competitions/${COMPETITION}/teams`,
  team: `competitions/${COMPETITION}/teams/`
}
let API_ID = '/';

const saveCachedValue = function(doc, value) {
  value.path = API_ID // Ensure we store the path for indexing/caching
  value.storedAt = new Date().getTime();
  const item = {
    data: value,
  }
  return client
    .query(q.Create(q.Collection('cache'), item))
    .then((response) => {
      console.log('Successfully stored cache for path', API_ID);
    })
    .catch((error) => {
      console.log('error', error)
    })
}

const deleteOldCache = async () => {
  const response = await client.query(q.Get(q.Match(q.Index('path'), API_ID)));
  // Delete old cache.
  await client.query(q.Delete(q.Ref(q.Collection('cache'), `${response.ref.id}`)));
}

const handler = async function (event, context) {
  client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
  });
  const {identity, user} = context.clientContext;
  const api = event.queryStringParameters.api;
  const params = event.queryStringParameters.params;
  const pathParam = event.queryStringParameters.pathParam;
  const path = API_PATHS[api] + (pathParam ? `${pathParam}` : '') + (params ? `?${params}` : '');
  console.log('Path is:', path);
  API_ID = path;
  let cachedValue = undefined
  let storedAt = undefined;
  const now = new Date().getTime();
  const response = await client
    .query(q.Get(q.Match(q.Index('path'), API_ID)))
    .then((response) => {
      cachedValue = {
        statusCode: 200,
        body: JSON.stringify(response.data),
      }
      storedAt = response.data.storedAt;
      return response;
    })
    .catch((error) => {
      console.log('error', error)
    })
    if (cachedValue !== undefined && (now - storedAt) <= INVALIDATE_AFTER) {
      console.log('returning cached value!');
      return cachedValue
    }
  
  try {
    if (cachedValue !== undefined) {
      console.log('More than 5 minutes has passed, delete old cache');
      await deleteOldCache();
    }
    
    const url = `https://api.football-data.org/v2/${API_ID}`;
    const params = {
      headers: {
        'X-Auth-Token': process.env.FOOTBALL_API_TOKEN
      }
    }
    console.log('fetch football api', url);
    const response = await fetch(url, params);
    if (!response.ok) {
      // NOT res.status >= 200 && res.status < 300
      return { statusCode: response.status, body: response.statusText }
    }
    const data = await response.json()
    // Save cached value
    cachedValue = {
      statusCode: 200,
      body: JSON.stringify(data),
    }
    await saveCachedValue(API_ID, data)
    return cachedValue
  } catch (error) {
    // output to netlify function log
    console.log(error)
    return {
      statusCode: 500,
      // Could be a custom message or object i.e. JSON.stringify(err)
      body: JSON.stringify({ msg: error.message }),
    }
  }
  finally {
    console.log('football close client');
    client.close().finally(); // Not waiting for the close connection.
  }
}

module.exports = { handler }
