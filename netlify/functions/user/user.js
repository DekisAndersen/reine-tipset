const process = require('process')
const { query, Client } = require('faunadb')
// FQL functions
const {
  Get,
  Match,
  Exists,
  Index,
  Create,
  Collection,
} = query;

async function getUserFromDB(user) {
  console.log('Checking DB..');
  const userExists = await client.query(Exists(Match(Index('users_by_id'), `${user.sub}`)));
  console.log('User exists', userExists);
  let dbUser;
  if (!userExists) {
    const item = {
      data: user
    }
    console.log('Create user..');
    dbUser = await client.query(Create(Collection('users'), item));
  }
  else {
    dbUser = await client.query(Get(Match(Index('users_by_id'), `${user.sub}`)));
  }
  return dbUser;
}


const handler = async function(event, context) {
  console.log('DB user - protected function!')
  client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET
  });
  // Reading the context.clientContext will give us the current user
  const user = context.clientContext && context.clientContext.user
  
  if (!user) {
    console.log('No claims! Begone!')
    return {
      statusCode: 401,
      body: JSON.stringify({
        data: 'NOT ALLOWED'
      })
    }
  }
  
  try {
    const dbUser = await getUserFromDB(user);
    return {
      statusCode: 200,
      body: JSON.stringify({
        data: dbUser
      })
    }
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: 500,
      body: JSON.stringify({
        data: err
      })
    }
  } finally {
    console.log('create user client close');
    client.close().finally(); // Not waiting for the close connection.
  }
  
}


module.exports = { handler }
