const process = require('process')
const { query, Client } = require('faunadb')
// FQL functions
const {
  Ref,
  Paginate,
  Get,
  Match,
  Select,
  Exists,
  Index,
  Update,
  Create,
  Collection,
  Join,
  Call,
  Function: Fn,
} = query;

const DEADLINE = new Date('2021-06-16T13:00:00Z');

let client = null

async function getUserFromDB(user) {
  const userExists = await client.query(Exists(Match(Index('users_by_id'), `${user.sub}`)));
  let dbUser;
  if (!userExists) {
    console.log('create user');
    const item = {
      data: user
    }
    dbUser = await client.query(Create(Collection('users'), item));
    console.log('user created');
  }
  else {
    return await client.query(Get(Match(Index('users_by_id'), `${user.sub}`)));
  }
}

const storeWinner = async (user, team) => {
  const now = new Date().getTime();
  if (now > DEADLINE.getTime()) {
    return 400;
  }
  let dbUser = await getUserFromDB(user);
  dbUser.data = {
    ...dbUser.data,
    winner: team
  }
  const res = await client.query(Update(Ref(Collection('users'), `${dbUser.ref.id}`), {data: dbUser.data}));
  
  return res;
};

const handler = async function (event, context) {
  console.log('User winner - protected function!')
  client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
  });
  // Reading the context.clientContext will give us the current user
  const user = context.clientContext && context.clientContext.user
  console.log('user claims', user)
  
  if (!user) {
    console.log('No claims! Begone!')
    return {
      statusCode: 401,
      body: JSON.stringify({
        data: 'NOT ALLOWED',
      }),
    }
  }
  
  const data = JSON.parse(event.body)
  try {
    const response = await storeWinner(user, data);
    if (response === 400) {
      return {
        statusCode: 400,
        body: JSON.stringify({
          data: 'Too late, competition has started!'
        })
      }
    }
    return {
      statusCode: 200,
      body: JSON.stringify({
        data: response.data.winner,
      }),
    }
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: 500,
      body: JSON.stringify({
        data: err
      }),
    }
  }
  
  
}

module.exports = { handler }
