const process = require('process')
const { query, Client } = require('faunadb')
// FQL functions
const {
  Ref,
  Paginate,
  Get,
  Match,
  Select,
  Exists,
  Index,
  Update,
  Create,
  Collection,
  Join,
  Call,
  Function: Fn,
} = query;

let client = null;

async function getUserFromDB(user) {
  const userExists = await client.query(Exists(Match(Index('users_by_id'), `${user.sub}`)));
  console.log('user exists', userExists);
  let dbUser;
  if (!userExists) {
    console.log('create user');
    const item = {
      data: user
    }
    dbUser = await client.query(Create(Collection('users'), item));
    console.log('user created');
  }
  else {
    return await client.query(Get(Match(Index('users_by_id'), `${user.sub}`)));
  }
}

const storeWinner = async (user, team) => {
  let dbUser = await getUserFromDB(user);
  dbUser.data = {
    ...dbUser.data,
    winner: team
  }
  const res = await client.query(Update(Ref(Collection('users'), `${dbUser.ref.id}`), {data: dbUser.data}));
  return res;
};

const readWinner = async (user) => {
  const dbUser = await client.query(Get(Match(Index('users_by_id'), `${user.sub}`)));
  return dbUser.data.winner;
}

const handler = async function (event, context) {
  console.info('User winner/read - protected function!')
  client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
  });
  // Reading the context.clientContext will give us the current user
  const user = context.clientContext && context.clientContext.user
  console.info('user claims', user)
  
  if (!user) {
    return {
      statusCode: 401,
      body: JSON.stringify({
        data: 'NOT ALLOWED',
      }),
    }
  }
  
  try {
    const winn = await readWinner(user);
    return {
      statusCode: 200,
      body: JSON.stringify({
        data: winn,
      }),
    }
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: 500,
      body: JSON.stringify({
        data: err
      }),
    }
  }
  
  
}

module.exports = { handler }
