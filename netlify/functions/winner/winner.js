const createRoute = require('./create')
//const deleteRoute = require('./delete')
const readRoute = require('./read')
//const readAllRoute = require('./read-all')
//const updateRoute = require('./update')

const handler = async (event, context) => {
  const path = event.path.replace('api/winner', '')
  const segments = path.split('/').filter(Boolean)
  switch (event.httpMethod) {
    case 'GET':
      const [id] = segments
      event.id = id
      return readRoute.handler(event, context)
    case 'POST':
      return createRoute.handler(event, context)
    case 'PUT':
      if (segments.length === 1) {
        const [id] = segments
        event.id = id
        return updateRoute.handler(event, context)
      }
      return {
        statusCode: 500,
        body: 'invalid segments in POST request, must be /.netlify/functions/fauna-crud/123456',
      }

    case 'DELETE':
      if (segments.length === 1) {
        const [id] = segments
        event.id = id
        return deleteRoute.handler(event, context)
      }
      return {
        statusCode: 500,
        body: 'invalid segments in DELETE request, must be /.netlify/functions/fauna-crud/123456',
      }
    default:
      return {
        statusCode: 500,
        body: 'unrecognized HTTP Method, must be one of GET/POST/PUT/DELETE',
      }
  }
}

module.exports = { handler }
