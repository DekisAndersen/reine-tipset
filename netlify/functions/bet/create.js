const process = require('process')
const { query, Client } = require('faunadb')
const fetch = require('node-fetch');
// FQL functions
const {
  Ref,
  Get,
  Match,
  Exists,
  Index,
  Update,
  Create,
  Collection,
} = query;

const BASE_URL = process.env.BASE_URL

let client = null;

async function getUserFromDB(user) {
  const userExists = await client.query(Exists(Match(Index('users_by_id'), `${user.sub}`)));
  let dbUser;
  if (!userExists) {
    const item = {
      data: user
    }
    dbUser = await client.query(Create(Collection('users'), item));
  }
  else {
    return await client.query(Get(Match(Index('users_by_id'), `${user.sub}`)));
  }
}

function generateHeaders(token) {
  const headers = { "Content-Type": "application/json" };
  return { ...headers, Authorization: `Bearer ${token}` };
}

const storeBet = async (user, bet) => {
  let dbUser = await getUserFromDB(user);
  const data = {
    user: dbUser.ref.id,
    match: bet.match,
    bet
  }
  
  try {
    const matchBet = await client.query(Get(Match(Index('bet_by_user_match'), data.user, data.match)));
    matchBet.data.bet = {...matchBet.data.bet, ...bet};
    await client.query(Update(Ref(Collection('bets'), matchBet.ref.id), {data: matchBet.data}));
    return matchBet.data;
  }
  catch (err){
    console.log('Not found, create');
  }
  
  const res = await client.query(Create(Collection('bets'), {data}));
  return res.data;
};

const matchValid = async function (matchId, token) {
  const headers = generateHeaders(token);
  const response = await fetch(BASE_URL + `/api/football?api=match&pathParam=${matchId}`, {
    headers
  })
  if (!response.ok) {
    console.error(BASE_URL + `/api/football?api=match&pathParam=${matchId}`, { statusCode: response.status, body: response.statusText })
    return false;
  }
  
  const data = await response.json();
  return data.match.status === 'SCHEDULED';
}

const handler = async function (event, context) {
  console.log('User match bet - protected function!')
  client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
  });
  // Reading the context.clientContext will give us the current user
  const user = context.clientContext && context.clientContext.user
  
  if (!user) {
    console.log('No claims! Begone!')
    return {
      statusCode: 401,
      body: JSON.stringify({
        data: 'NOT ALLOWED',
      }),
    }
  }
  
  const data = JSON.parse(event.body)
  const isValid = await matchValid(data.match, context.clientContext.identity.token);
  if (!isValid) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        data: 'NO HACKING OR CHEATING!'
      })
    }
  }
  
  try {
    const bet = await storeBet(user, data);
    return {
      statusCode: 200,
      body: JSON.stringify({
        data: bet,
      }),
    }
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: 500,
      body: JSON.stringify({
        data: err
      }),
    }
  }
  finally {
    console.log('bet create close client');
    client.close().finally(); // Not waiting for the close connection.
  }
}

module.exports = { handler }
