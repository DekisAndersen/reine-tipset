const process = require('process')
const { query, Client } = require('faunadb')
// FQL functions
const {
  Ref,
  Paginate,
  Get,
  Match,
  Select,
  Exists,
  Index,
  Update,
  Create,
  Collection,
  Join,
  Call,
  Function: Fn,
} = query;

let client = null;

const getMatchBets = async (matchId) => {
  const matchBets = await client.query(Paginate(Match(Index('bets_per_match'), matchId)));
  const itemRefs = matchBets.data;
  // create new query out of item refs.
  const getAllItemsDataQuery = itemRefs.map((ref) => query.Get(ref));
  // then query the refs
  const bets = await client.query(getAllItemsDataQuery)
  const getAllUsersQuery = bets.map((bet) => Get(Ref(Collection('users'), `${bet.data.user}`)));
  const allUsers = await client.query(getAllUsersQuery);
  const result = [];
  bets.forEach(bet => {
    const user = allUsers.find(user => user.ref.id === bet.data.user);
    const data = {
      user: user.data,
      bet: bet.data
    }
    result.push(data);
  });
  return result;
}

const handler = async function (event, context) {
  client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
  });
  // Reading the context.clientContext will give us the current user
  const user = context.clientContext && context.clientContext.user
  
  if (!user) {
    return {
      statusCode: 401,
      body: JSON.stringify({
        data: 'NOT ALLOWED',
      }),
    }
  }
  
  try {
    const matchId = Number(event.id);
    if (Number.isNaN(matchId)) {
      return {
        statusCode: 400,
        body: JSON.stringify({data: 'Could not parse match id'})
      }
    }
    console.log('get bets for ', matchId);
    const matchBets = await getMatchBets(matchId);
    return {
      statusCode: 200,
      body: JSON.stringify({
        data: matchBets,
      }),
    }
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: 500,
      body: JSON.stringify({
        data: err
      }),
    }
  }
  finally {
    console.log('bet read close client');
    client.close().finally(); // Not waiting for the close connection.
  }
  
  
}

module.exports = { handler }
