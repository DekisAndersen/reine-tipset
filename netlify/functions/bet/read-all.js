/* Import faunaDB sdk */
const process = require('process')
const { query, Client } = require('faunadb')
// FQL functions
const {
  Ref,
  Paginate,
  Get,
  Match,
  Select,
  Exists,
  Index,
  Update,
  Create,
  Collection,
  Join,
  Call,
  Function: Fn,
} = query;

let client = null;

async function getUserFromDB(user) {
  const userExists = await client.query(Exists(Match(Index('users_by_id'), `${user.sub}`)));
  let dbUser;
  if (!userExists) {
    const item = {
      data: user
    }
    dbUser = await client.query(Create(Collection('users'), item));
  }
  else {
    return await client.query(Get(Match(Index('users_by_id'), `${user.sub}`)));
  }
}

const handler = async function (event, context) {
  client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
  });
  // Reading the context.clientContext will give us the current user
  const user = context.clientContext && context.clientContext.user
  
  if (!user) {
    return {
      statusCode: 401,
      body: JSON.stringify({
        data: 'NOT ALLOWED',
      }),
    }
  }
  
  try {
    const dbUser = await getUserFromDB(user);
    const response = await client.query(query.Paginate(query.Match(query.Index('bets_by_user'), `${dbUser.ref.id}`)))
    const itemRefs = response.data
    // create new query out of item refs. http://bit.ly/2LG3MLg
    const getAllItemsDataQuery = itemRefs.map((ref) => query.Get(ref))
    // then query the refs
    const ret = await client.query(getAllItemsDataQuery)
    return {
      statusCode: 200,
      body: JSON.stringify(ret.map(item => item.data.bet)),
    }
  } catch (error) {
    console.log('error', error)
    return {
      statusCode: 400,
      body: JSON.stringify(error),
    }
  }
  finally {
    console.log('Bet read all close client');
    client.close().finally()
  }
}

module.exports = { handler }
