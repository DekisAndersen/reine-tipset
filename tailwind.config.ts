import {defineConfig} from 'vite-plugin-windicss'
import colors from 'windicss/colors'
import typography from 'windicss/plugin/typography'
import forms from 'windicss/plugin/forms'

export default defineConfig({
  darkMode: 'class',
  // corePlugins: {
  //   accessibility: true
  // },
  plugins: [
    typography(),
    forms
  ],
  theme: {
    ringColor: {
      green: '053e36',
      'r-green': '64edaa',
    },
    extend: {
      colors: {
        'r-green': '#64edaa',
        'green-fade': '#7bf0b7',
        'dark-green-bg': '#264B44',
        'dark-green-input-border' : '#4D6D68',
        'dark-green': '#053e36',
        'darker-green': '#033730',
        'dark-green-fade': '#205648',
      },
      boxShadow: {
        DEFAULT: '0 2px 4px rgba(0, 0, 0, .12), 0 0 6px rgba(0, 0, 0, .04)'
      },
      gridTemplateColumns: {
        'auto-fill': 'repeat(auto-fill, minmax(0, 1fr))',
      },
      typography: {
        DEFAULT: {
          css: {
            maxWidth: '65ch',
            color: 'inherit',
            a: {
              color: 'inherit',
              opacity: 0.75,
              fontWeight: '500',
              textDecoration: 'underline',
              '&:hover': {
                opacity: 1,
                color: '#64edaa'
              },
            },
            b: { color: 'inherit' },
            strong: { color: 'inherit' },
            em: { color: 'inherit' },
            h1: { color: 'inherit' },
            h2: { color: 'inherit' },
            h3: { color: 'inherit' },
            h4: { color: 'inherit' },
            code: { color: 'inherit' },
          },
        },
      },
    },
  },
})
