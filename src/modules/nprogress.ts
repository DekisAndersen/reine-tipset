import NProgress from 'nprogress'
import {Router} from "vue-router";
// import { UserModule } from '~/types'

export const installNProgress: (router: Router) => void = ( router ) => {
    router.beforeEach(() => { NProgress.start() })
    router.afterEach(() => { NProgress.done() })
}
