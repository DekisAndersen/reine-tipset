export type Bet = {
    match: Number,
    home: String,
    away: String
}

export type Score = {homeTeam: Number | null, awayTeam: Number | null}
export type Team = {id: Number, name: String}

export type Match = {
    id: Number,
    season: Object,
    utcDate: Date
    status: String,
    matchday: Number,
    stage: String,
    group: String,
    lastUpdated: Date
    odds: Object,
    score: {
        winner: Object,
        duration: String,
        fullTime: Score,
        halfTime: Score,
        extraTime: Score,
        penalties: Score
    },
    homeTeam: Team,
    awayTeam: Team,
}
