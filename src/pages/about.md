---
title: Reine-Tipset About
---

<div class="text-center">
  <!-- You can use Vue components inside markdown -->
  <carbon-information class="text-4xl -mb-6 m-auto" />
  <h3>Reine-Tipset Information</h3>
</div>

 
## About this project

The continued evolution of the Reine-Tips tradition. Previous iterations looked something like this:

<p align='center'>
  <img src='https://i.imgur.com/R5OarHk.png' alt='Reine VM-Tips 2018' width='600'/>
</p>

### History

The VM/EM-Tips has been a pillar of Reine-tradition since it's first iteration 2014 - Then in collaboration with our two co-companies at that office. 

Historically we've had >90% participation but, you know, it's optional. Even if you don't know anything about football, taking wild guesses is a very valid strategy that usually works out pretty 
well! 

### Purpose

You'll guess the winner of the competition as well as individual matches and get points. Points means a highscore table, which means winners, which means prizes!
Make (un)educated guesses for outcomes and take part in the mass hysteria that is football. Maybe cheer on your favourite team! 

Read more under the [Rules](/rules) section.


### Techy nerd-stuff <carbon-laptop />

<teenyicons-bitbucket-outline /> Project Repo: [Bitbucket](https://bitbucket.org/DekisAndersen/reine-tipset/src/master/).

<carbon-data-base /> Database: [FaunaDB](https://fauna.com/)

<carbon-api /> API:  Football API from [football-data.org](https://www.football-data.org/)

<carbon-code /> Code: See [Code stack](#Code-stack) below

<carbon-hotel /> Hosting: [Netlify](https://www.netlify.com/)



#### Code stack
The project is built using [Vitesse](https://github.com/antfu/vitesse) - an opinionated [Vite](https://github.com/vitejs/vite) starter template made by [@antfu](https://github.com/antfu).

- ⚡️ [Vue 3](https://github.com/vuejs/vue-next), [Vite 2](https://github.com/vitejs/vite), [pnpm](https://pnpm.js.org/), [ESBuild](https://github.com/evanw/esbuild) - born with fastness

- 🗂 [File based routing](https://github.com/hannoeru/vite-plugin-pages)

- 📦 [Components auto importing](https://github.com/antfu/vite-plugin-components)

- 📑 [Layout system](https://github.com/hannoeru/vite-plugin-pages)

- 📲 [PWA](https://github.com/antfu/vite-plugin-pwa)

- 🎨 [Windi CSS](https://github.com/windicss/windicss) - on-demand Tailwind CSS with speed

- 😃 [Use icons from any icon sets, with no compromise](https://github.com/antfu/vite-plugin-icons)

- 🌍 [I18n ready](https://github.com/intlify/vue-i18n-next)

- 🗒 [Markdown Support](https://github.com/antfu/vite-plugin-md)

- 🔥 Use the [new `<script setup>` style](https://github.com/vuejs/rfcs/pull/227)

- 🖨 Server-side generation (SSG) via [vite-ssg](https://github.com/antfu/vite-ssg)

- 🦾 TypeScript

- ☁️ Deploy on Netlify, zero-config



Check out the [GitHub repo](https://github.com/antfu/vitesse) for more details.
