---
title: Reine-Tipset Rules
---

<div class="text-center">
  <carbon-ruler class="text-4xl -mb-6 m-auto" />
  <h3>Reine-Tipset Rules</h3>
</div>

 

## Predict the winner <carbon-trophy />
From the [Home](/) screen, choose 1 team you think will win the Euro 2020 championship. 
Everyone correctly guessing the winning team gains **10 points**.

~~You have until the competition starts to pick a winner!~~ 
New for this year; It will be possible to predict the winner up until the second round starts, meaning 2021-06-16 @ 13:00:00 UTC.

This way you'd have had the chance to see each team play at least 1 game.

## Place the bets <carbon-ticket />
Up until a match starts, you can bet on what you think the final score will be.  
_So you don't need to fill in all matches beforehand. A game starting at 18/06/2021, 21:00:00 can be entered/changed up until 18/06/2021, 20:59:59_

The final score will be determined after ordinary full time.
So if e.g. the final game ends 2-2 and then goes on to extra time / penalties the counting result will still be 2-2. 

If you bet the correct winner: Home, Away or Draw, you gain **1 point**.

_For example you guessed 1-3, the game ended 0-1; you'll get 1 point_

If you bet the correct exact result, you gain **3 points**.

## Prizes 
All participants add to the prize pool for something around 50-100:- SEK. The traditional go-to has been a bottle of wine but non-alcoholic options are of course welcome; perhaps a box of 
chocolate.
The spoils of war are then divided amongst the top [highscorers](/highscore) after the competition. Depending on turnout this can be top 3, or top 5 or more. Updates will come after the competition starts. 

