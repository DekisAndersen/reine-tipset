import 'windi.css'
import './styles/main.css'
import generatedRoutes from 'pages-generated'
import {setupLayouts} from 'layouts-generated'
import App from './App.vue'
import {useState} from "~/logics/store";
import {createApp} from "vue";
import {createHead} from "@vueuse/head";
import {createRouter, createWebHistory} from "vue-router";
import {installI18n} from "~/modules/i18n";
import {installNProgress} from "~/modules/nprogress";

const { state } = useState();

const routes = setupLayouts(generatedRoutes)
const router = createRouter({
    history: createWebHistory(),
    routes,
    scrollBehavior: ((to, from, savedPosition) => {
        if (to.hash) {
            return {el: to.hash}
        }
    })
});
router.beforeEach((to, from, next) => {
    const authRequired = to.matched.some(route => route.meta.auth);
    if (!authRequired) {
        return next();
    }
    // check if current user
    if (state.value.currentUser) {
        return next()
    }
    return next({name: "login", query: {redirectFrom: to.fullPath}});
});

const head = createHead();
const app = createApp(App);
app.use(head).use(router).mount('#app');
installI18n(app);
installNProgress(router);
