import netlifyIdentity from "netlify-identity-widget";
import {Bet} from "~/types";

async function generateHeaders() {
    const headers = {"Content-Type": "application/json"};
    if (netlifyIdentity.currentUser()) {
        // @ts-ignore
        const token = await netlifyIdentity.currentUser().jwt()
        return {...headers, Authorization: `Bearer ${token}`};
    }
    return headers;
}

// From https://api.football-data.org/v2/competitions/EC/matches
async function fetchUpcomingMatches() {
    const headers = await generateHeaders();
    const data = await fetch('/api/football?api=matches', {
        headers
    }).catch(err => {throw new Error(err)});
    const json = await data.json();
    return json;
}

// From https://api.football-data.org/v2/competitions/EC/matches?dateFrom[..]&dateTo[..]
async function fetchTodaysMatches() {
    const now = new Date();
    const isoDate = now.toISOString().split('T')[0];
    const headers = await generateHeaders();
    const data = await fetch(`api/football?api=matches&params=dateFrom=${isoDate}%26dateTo=${isoDate}`, {headers})
        .catch(err => {throw new Error(err)});
    const json = await data.json();
    return json.matches;
}

// From https://api.football-data.org/v2/competitions/EC/standings
async function fetchStandings() {
    const headers = await generateHeaders();
    const data = await fetch('/api/football?api=standings', {
        headers
    }).catch(err => {throw new Error(err)});
    let json = await data.json();
    return json;
}

// From https://api.football-data.org/v2/competitions/EC/teams
async function fetchTeams() {
    const headers = await generateHeaders();
    const data = await fetch('/api/football?api=teams', {
        headers
    }).catch(err => {throw new Error(err)});
    let json = await data.json();
    return json;
}

// From https://api.football-data.org/v2/teams/{ID}
// async function fetchTeam(teamId) {
//     const headers = await generateHeaders();
//     const data = await fetch(`/api/football?api=team&pathParam=${teamId}`, {
//         headers
//     }).catch(err => {throw new Error(err)});
//     let json = await data.json();
//     return json;
// }

async function storeUserWinner(team) {
    const headers = await generateHeaders();
    const {id, name, shortName, tla} = team;
    const response = await fetch('/api/winner/', {
        body: JSON.stringify({id, name, shortName, tla}),
        method: 'POST',
        headers
    }).catch(err => {throw new Error(err)});
    return response;
}

async function fetchUserWinner() {
    const headers = await generateHeaders();
    const response = await fetch('/api/winner', {
        headers
    })
        .then(response => response.json())
        .then(result => result.data)
        .catch(err => {throw new Error(err)});
    return response;
}

async function storeUserMatchBet(bet: Bet) {
    const headers = await generateHeaders();
    await fetch('/api/bet', {
        headers,
        method: 'POST',
        body: JSON.stringify(bet)
    }).catch(err => {throw new Error(err)});
    return bet;
}

async function fetchAllUserMatchBets() {
    const headers = await generateHeaders();
    const response = await fetch('/api/bet', {headers})
        .then(response => response.json())
        .catch(err => {throw new Error(err)});
    return response;
}

async function fetchAllMatchBets(matchId: Number) {
    const headers = await generateHeaders();
    const response = await fetch(`/api/bet/${matchId}`, {headers})
        .then(response => response.json())
        .catch(err => {
            throw new Error(err)
        });
    return response.data;
}

async function updateHighscores() {
    const headers = await generateHeaders();
    const response = await fetch('/api/highscore', {
        headers,
        method: 'POST',
        body: JSON.stringify("")
    })
        .then(response => response.json())
        .catch(err => {
            throw new Error(err)
        });
    return response;
}

async function updateDBUser() {
    const headers = await generateHeaders();
    const response = await fetch('/api/user', {
        headers,
        method: 'POST',
        body: JSON.stringify("")
    })
        .then(response => response.json())
        .catch(err => {
            throw new Error(err)
        });
    return response.data;
}

function getFlagUrl(teamId: number) {
    return 'https://crests.football-data.org/' + teamId + '.svg'
}


export {
    fetchUpcomingMatches,
    fetchTodaysMatches,
    fetchStandings,
    fetchTeams,
    getFlagUrl,
    storeUserWinner,
    fetchUserWinner,
    storeUserMatchBet,
    fetchAllUserMatchBets,
    fetchAllMatchBets,
    updateHighscores,
    updateDBUser
}
