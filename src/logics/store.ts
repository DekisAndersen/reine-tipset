import {computed, ref, Ref} from 'vue'
import {useStorage} from "@vueuse/core";
import netlifyIdentity, {User} from "netlify-identity-widget";

netlifyIdentity.init();
const currentUser = netlifyIdentity.currentUser();
const state = ref({currentUser});
export function useState() {
    function setUser(user: User | null) {
        state.value.currentUser = user;
    }
    return {
        state: computed(() => state.value),
        setUser
    }
}

/* Sidebar */
export const SIDEBAR_WIDTH = 180
export const SIDEBAR_WIDTH_COLLAPSED = 54

export const collapsed = useStorage('sidebar-collapse', false) as Ref<boolean>
export const toggleSidebar = () => (collapsed.value = !collapsed.value)

export const sidebarWidth = computed(
    () => `${collapsed.value ? SIDEBAR_WIDTH_COLLAPSED : SIDEBAR_WIDTH}px`
)

export const colorSchema = useStorage('color-schema', 'auto') as Ref<'auto' | 'dark' | 'light'>
