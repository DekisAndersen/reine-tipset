import {Bet, Match} from "~/types";

const calcMatchScore = (bet: Bet, match: Match) => {
    if (match.status === 'SCHEDULED') {
        return null;
    }
    if (bet == null) {
        return 0;
    }
    const betHome = Number(bet.home);
    const betAway = Number(bet.away);
    if (Number.isNaN(betHome) || Number.isNaN(betAway)) {
        return 0;
    }
    let matchHome = match.score.fullTime.homeTeam as number;
    let matchAway = match.score.fullTime.awayTeam as number;
    if (match.score.extraTime.homeTeam != null) {
        const etHome = match.score.extraTime.homeTeam as number;
        const etAway = match.score.extraTime.awayTeam as number;
        matchHome = matchHome - etHome;
        matchAway = matchAway - etAway;
    }
    if (match.score.penalties.homeTeam != null) {
        const pHome = match.score.penalties.homeTeam as number;
        const pAway = match.score.penalties.awayTeam as number;
        matchHome = matchHome - pHome;
        matchAway = matchAway - pAway;
    }

    if (betHome == matchHome && betAway == matchAway) {
        return 3;
    }
    return Math.sign(betHome - betAway) === Math.sign(matchHome - matchAway) ? 1 : 0;
}


export {
    calcMatchScore
}
